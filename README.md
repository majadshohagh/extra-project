# runtime download text file or csv or any format
### just change this line of code extenstion, and content-type
```php
    $textData = "ID,".__('Klient-Nr.').",".__('patient.PatientList.patient_name').",Street Line 1,Postal Code/ZIP,Country\n";
        foreach($patients as $k => $datum){
            
            $textData .= ++$k.",".ucfirst($datum->client_no).",". trim(ucfirst($datum->first_name)).' '.ucfirst(trim($datum->last_name)).",". $datum->road .",". $datum->plz .",". $datum->place."\n";
        }

        // file name that will be used in the download
        $fileName = $billingName.".txt";

        // use headers in order to generate the download
        $headers = [
        'Content-type' => 'text/plain', 
        'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
        // 'Content-Length' => sizeof($patients)
        ];

        // make a response, with the content, a 200 response code and the headers
        return Response::make($textData, 200, $headers);
    }
```

## laravel folder & file zip function goes headers

```php copy
        $zip = new \ZipArchive();
            $fileName = $getMonthYear[0]."_".$getMonthYear[1].'.zip';
            if ($zip->open(public_path($fileName), \ZipArchive::CREATE)== TRUE)
            {
                $files = File::files(public_path($path));
                foreach ($files as $key => $value){
                    $relativeName = basename($value);
                    $zip->addFile($value, $relativeName);
                }
                $zip->close();
            }

        return response()->download(public_path($fileName));
```
